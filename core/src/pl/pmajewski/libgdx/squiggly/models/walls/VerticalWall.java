package pl.pmajewski.libgdx.squiggly.models.walls;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;

import pl.pmajewski.libgdx.squiggly.box2d.UserData;
import pl.pmajewski.libgdx.squiggly.models.BallModel;
import pl.pmajewski.libgdx.squiggly.models.GameActor;
import pl.pmajewski.libgdx.squiggly.utils.GlobalProperties;

public abstract class VerticalWall extends GameActor {

    protected BallModel ball;
    private Vector2 startPosition;

    public VerticalWall(World world, BallModel ballModel, Vector2 startPosition) {
        super(world);
        this.ball = ballModel;
        this.startPosition = startPosition;
        initBody(world);
    }

    @Override
    public void act(float delta) {
        body.setTransform(body.getPosition().x, ball.getBody().getPosition().y, 0);
        super.act(delta);
    }

    private void initBody(World world) {
        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.StaticBody;
        bodyDef.position.set(startPosition);

        PolygonShape shape = new PolygonShape();
        shape.setAsBox(GlobalProperties.SCENE_HORIZONTAL_WALL_WIDTH/2, GlobalProperties.SCENE_HORIZONTAL_WALL_HEIGHT/2);

        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = shape;
        fixtureDef.density = GlobalProperties.WALL_DENSITY;
        fixtureDef.restitution = 1f;

        UserData userData = new UserData(this, GlobalProperties.SCENE_HORIZONTAL_WALL_WIDTH, GlobalProperties.SCENE_HORIZONTAL_WALL_HEIGHT/2);
        Body body = world.createBody(bodyDef);
        Fixture fixture = body.createFixture(fixtureDef);
        fixture.setUserData(userData);
        body.setUserData(userData);
        body.resetMassData();

        shape.dispose();
        this.body = body;
    }
}
