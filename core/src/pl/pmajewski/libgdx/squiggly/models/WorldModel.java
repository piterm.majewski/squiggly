package pl.pmajewski.libgdx.squiggly.models;

import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.World;

import lombok.Getter;
import pl.pmajewski.libgdx.squiggly.utils.GlobalProperties;

@Getter
public class WorldModel {

    private ContactListener listener;
    private World world;

    public WorldModel(ContactListener listner) {
        this.listener = listner;
        setUpWorld();
    }

    private void setUpWorld() {
        this.world = new World(GlobalProperties.WORLD_GRAVITY, true);
        world.setContactListener(this.listener);
        World.setVelocityThreshold(0f);
    }
}
