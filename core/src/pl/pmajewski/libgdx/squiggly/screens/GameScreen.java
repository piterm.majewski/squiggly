package pl.pmajewski.libgdx.squiggly.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;

import pl.pmajewski.libgdx.squiggly.stages.GameStage;

public class GameScreen extends ScreenAdapter {

    private GameStage stage = new GameStage();

    @Override
    public void render(float delta) {
        super.render(delta);

        Gdx.gl.glClearColor(1f/255f*50f, 1f/255f*50f, 1f/255f*50f, 1f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        Gdx.gl.glEnable(GL20.GL_SAMPLES);

        stage.draw();
        stage.getDebugRender().render(stage.getWorldModel().getWorld(), stage.getCamera().combined);
        stage.act(delta);
    }
}
