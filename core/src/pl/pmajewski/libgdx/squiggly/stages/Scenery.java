package pl.pmajewski.libgdx.squiggly.stages;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Stage;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import lombok.Getter;
import pl.pmajewski.libgdx.squiggly.models.BallModel;
import pl.pmajewski.libgdx.squiggly.models.EnemyModel;
import pl.pmajewski.libgdx.squiggly.models.WorldModel;
import pl.pmajewski.libgdx.squiggly.utils.GlobalProperties;

public class Scenery {

    private Stage gameStage;
    private WorldModel worldModel;
    private float centerX;
    private Random rand = new Random();

    @Getter
    private BallModel ball;
    private List<EnemyModel> enemies = new LinkedList<>();

    public Scenery(WorldModel worldModel, Stage stage, float centerX) {
        this.worldModel = worldModel;
        this.gameStage = stage;
        this.centerX = centerX;
        init();
    }

    private void init() {
        setUpBall();
        setUpEnemies();
    }

    private void setUpEnemies() {
        float positionY = ball.getBody().getPosition().y + GlobalProperties.ENEMY_INITIAL_GAP;

        while(positionY <= (GlobalProperties.APP_HEIGHT + ball.getBody().getPosition().y)) {
            float positionX = randomXPosition();
            generateEnemy(positionX, randomYShift(positionY));

            positionY += GlobalProperties.ENEMY_GAP;
        }
    }

    private void setUpBall() {
        this.ball = new BallModel(this.worldModel.getWorld(), this.centerX);
        this.ball.setScenery(this);
        gameStage.addActor(ball);
    }

    public void randomizeEnemy() {
        float positionX = randomXPosition();
        float positionY = randomYShift(ball.getBody().getPosition().y+GlobalProperties.APP_HEIGHT);
        generateEnemy(positionX, positionY);
    }

    private float randomXPosition() {
        float positioner = Math.random() > 0.5d ? 1f : -1f;
        float shift = rand.nextFloat()*GlobalProperties.APP_WIDTH/2/2;
        return centerX + (shift*positioner);
    }

    private float randomYShift(float positionX) {
        float positioner = Math.random() > 0.5d ? 1f : -1f;
        float shift = rand.nextFloat()*GlobalProperties.ENEMY_MAX_Y_SHIFT;
        return positionX + (positioner*shift);
    }

    private void generateEnemy(float positionX, float positionY) {
        Vector2 position = new Vector2(positionX, positionY);
        EnemyModel enemy = new EnemyModel(worldModel.getWorld(), position);
        enemies.add(enemy);
        gameStage.addActor(enemy);
    }

    public void reverseBall() {
        ball.reverseXVelocity();
    }
}