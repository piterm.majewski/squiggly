package pl.pmajewski.libgdx.squiggly.stages;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.Manifold;

import pl.pmajewski.libgdx.squiggly.box2d.UserData;
import pl.pmajewski.libgdx.squiggly.models.EnemyModel;

public class GameContactListener implements ContactListener {

    @Override
    public void beginContact(Contact contact) {
        contact.setEnabled(false);
    }

    @Override
    public void endContact(Contact contact) {
        contact.setEnabled(false);
    }

    @Override
    public void preSolve(Contact contact, Manifold oldManifold) {
        Fixture fixtureA = contact.getFixtureA();
        Fixture fixtureB = contact.getFixtureB();
        UserData userDataA = (UserData) fixtureA.getUserData();
        UserData userDataB = (UserData) fixtureB.getUserData();

        if(userDataA.getGameActor() instanceof EnemyModel || userDataB.getGameActor() instanceof EnemyModel) {
            fixtureA.getBody().setLinearVelocity(new Vector2(0 , 0));
            fixtureB.getBody().setLinearVelocity(new Vector2(0 , 0));
        }
    }

    @Override
    public void postSolve(Contact contact, ContactImpulse impulse) {

    }
}
