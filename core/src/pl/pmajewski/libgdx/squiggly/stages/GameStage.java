package pl.pmajewski.libgdx.squiggly.stages;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.FillViewport;

import lombok.Getter;
import pl.pmajewski.libgdx.squiggly.box2d.UserData;
import pl.pmajewski.libgdx.squiggly.models.GameActor;
import pl.pmajewski.libgdx.squiggly.models.WorldModel;
import pl.pmajewski.libgdx.squiggly.models.walls.VerticalWallLeft;
import pl.pmajewski.libgdx.squiggly.models.walls.VerticalWallMiddle;
import pl.pmajewski.libgdx.squiggly.models.walls.VerticalWallRight;
import pl.pmajewski.libgdx.squiggly.utils.GlobalProperties;

public class GameStage extends Stage {

    private static final float VIEWPORT_WIDTH = GlobalProperties.APP_WIDTH;
    private static final float VIEWPORT_HEIGHT = GlobalProperties.APP_HEIGHT;

    private GameContactListener contactListener;
    @Getter
    private WorldModel worldModel;
    @Getter
    private Box2DDebugRenderer debugRender;
    private Scenery leftScenery;
    private Scenery rightScenery;

    private float accumulator = 0f;

    public GameStage() {
        super(new FillViewport(VIEWPORT_WIDTH, VIEWPORT_HEIGHT,
                new OrthographicCamera(VIEWPORT_WIDTH, VIEWPORT_HEIGHT)));
        Gdx.input.setInputProcessor(this);
        contactListener = new GameContactListener();
        worldModel = new WorldModel(contactListener);
        setUpCharacters();
        debugRender = new Box2DDebugRenderer(true, true, true, true, true, true);
    }

    private void setUpCharacters() {
        setUpScenery();
        setUpWalls();
    }

    private void setUpScenery() {
        leftScenery = new Scenery(this.worldModel, this, GlobalProperties.APP_WIDTH/2/2);
        rightScenery = new Scenery(this.worldModel, this, GlobalProperties.APP_WIDTH/2+(GlobalProperties.APP_WIDTH/2/2));
    }

    private void setUpWalls() {
        addActor(new VerticalWallLeft(this.worldModel.getWorld(), leftScenery.getBall()));
        addActor(new VerticalWallRight(this.worldModel.getWorld(), leftScenery.getBall()));
        addActor(new VerticalWallMiddle(this.worldModel.getWorld(), leftScenery.getBall()));
    }

    private void setCameraPosition() {
        getCamera().position.y = leftScenery.getBall().getY()+6;
    }

    @Override
    public void act(float delta) {
        setCameraPosition();
        super.act(delta);
        printDebug();

        this.accumulator += delta;
        if(accumulator >= delta) {
            worldModel.getWorld().step(GlobalProperties.TIME_STEP, 6, 2);
            this.accumulator -= GlobalProperties.TIME_STEP;
        }
    }

    private void printDebug() {
        if(GlobalProperties.ACTORS_DEBUG) {
            Array<Body> bodies = new Array<>(worldModel.getWorld().getBodyCount());
            worldModel.getWorld().getBodies(bodies);
            for (Body temp: bodies) {
                Object obj = temp.getUserData();
                if(obj instanceof UserData) {
                    UserData userData = (UserData) obj;
                    if(userData.getGameActor() instanceof GameActor) {
                        System.out.println(userData.getGameActor().getDebudData());
                    }
                }
            }
        }
    }

    @Override
    public boolean keyDown(int keyCode) {
        if(Input.Keys.A == keyCode) {
            leftScenery.reverseBall();
        }

        if(Input.Keys.L == keyCode) {
            rightScenery.reverseBall();
        }

        return super.keyDown(keyCode);
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        if(screenX < Gdx.graphics.getWidth()/2) {
            leftScenery.reverseBall();
        } else {
            rightScenery.reverseBall();
        }
        return super.touchDown(screenX, screenY, pointer, button);
    }
}